<?php
##################################################################################
#Licenced  software inCeCILL (http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt)#
##################################################################################
#CONFIG
define("ROOT", "qwerty.dzv.me/url"); #URL of website
define("NAME", "MOTAPURLS : My Own Tiny and Pretty URL Shortening"); #Name of website
#FONCTIONS
	#http://snipplr.com/view/20671/short-url-function/
$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_');
 
function int_to_alph($int) {
	global $chrs;
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}
 
function alph_to_int($alph) {
	global $chrs;
	$base = sizeof($chrs);
	for($i = 0, $int = 0; $i < strlen($alph); $i++) {
		$int += intval(array_search(substr($alph, strlen($alph) - $i - 1, 1), $chrs)) * pow($base, $i);
	}
	return $int;
}
function sanitize_output($buffer) {
    $a = array(
        '/\>[^\S ]+/s' =>'>',
        '/[^\S ]+\</s'=>'<',
        '/(\s)+/s'=>'\\1',
        '(\r\n|\n|\r|\t)'=>''
		);
	$buffer =trim($buffer);
    $buffer = preg_replace(array_keys($a), array_values($a), $buffer);
    return $buffer;
}

try {
    $bdd = new PDO('sqlite:data.sqlite');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 
	catch (PDOException $e) {
    echo "<p>Erreur : " . $e->getMessage() . "</p>";
    exit();
}
if(!file_exists(".htaccess")) {
	$bdd->query('CREATE TABLE "url" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "url" TEXT)');	
	$htaccess = fopen('.htaccess', "x+");
	fputs($htaccess, "<IfModule mod_rewrite.c> \n
RewriteEngine On\n
#RewriteBase ".ROOT."\n
RewriteCond %{REQUEST_FILENAME} !-f\n
RewriteCond %{REQUEST_FILENAME} !-d\n
RewriteRule ^(.*)$  index.php?id=$1 [L]\n
</IfModule>");
	fclose($htaccess);
}
if(isset($_GET['id'])) { #REDIRECTOR
	$retour = $bdd->query('SELECT * FROM url WHERE id="'.intval(alph_to_int($_GET['id'])).'"');
	$retour->setFetchMode(PDO::FETCH_BOTH);
	$donnees = $retour->fetch();
	if("http://".ROOT."/".int_to_alph($_GET['id']) != $donnees['url'] AND !isset($_GET['api'])) { #Eviter que l'adresse auquel on redirige soit l'adresse raccourci (Merci Scout123 pour le bug !)
		header("Location: ".$donnees['url']."");
	}
	elseif(isset($_GET['api'])) {
		$data = array(
			'id' => $_GET['id'],
			'url' => $donnees['url']
		);
		echo json_encode($data);
	}
	else{}
}

if(isset($_POST['url']) || isset($_GET['url'])) {
	$url = (isset($_POST['url'])) ? $_POST['url'] : $_GET['url'];
	$retour = $bdd->query('SELECT COUNT(*) AS nb_entry FROM url WHERE url="'.$url.'"'); #TESTED ON ITS PRESENT IN THE DATABASE
	$retour->setFetchMode(PDO::FETCH_BOTH);
	$donnees = $retour->fetch();
	if($donnees['nb_entry'] > 0) { #IF YES, it gives the address shortcut
			$retour = $bdd->query('SELECT * FROM url WHERE url="'.$url.'"');
			$retour->setFetchMode(PDO::FETCH_BOTH);
			$donnees = $retour->fetch();
			$urlshoorten = "http://".ROOT."/".int_to_alph($donnees['id']);
			if(isset($_GET['api'])) { echo $urlshoorten;}
	}
	else { #IF NOT, it is added
		$retour = $bdd->prepare('INSERT INTO url(url) VALUES(:url)');
		$retour = $retour->execute( array( 'url' => $url) );
		$dernierid = $bdd->lastInsertId();
		$urlshoorten = "http://".ROOT."/".int_to_alph($dernierid);
		if(isset($_GET['api'])) { echo $urlshoorten;}
	}
	
}
header('Content-Type: text/html; charset=UTF-8');
ob_start("sanitize_output");
if(!isset($_GET['api'])) {
?>
<!doctype html>
<html>
	<head>
		<title><?php echo NAME; ?></title>
		<style>
*{font-family:proxima-nova, sans-serif;}
body{background:#fff;}
.box{position:relative;background-color:#FFF;width:300px;height:250px;z-index:1;margin:150px auto;}
input[type=url]{appearance:none;width:380px;height:70px;font-size:3em;background:transparent;border:none;border-bottom:solid 4px #e6e8eb;text-align:center;transition:border-color .6s ease;}
input[type=url][placeholder]{color:#CCC;}
input[type=url]:focus{outline:none;border:none;border-bottom:solid 4px #00AEEF;transition:border-color 1s ease;}
input[type=submit]{appearance:none;width:380px;height:50px;font-size:1.05em;background:transparent;color:#CCC;text-align:center;letter-spacing:.15em;text-transform:uppercase;transition:color .5s ease;outline:none;border:none;}
input[type=submit]:hover{color:#464c4c;cursor:pointer;transition:color .5s ease;}
h1{text-align:center;color:#464c4c;}
.alert{text-shadow:0 1px 0 rgba(255,255,255,0.5);border:1px solid #fbeed5;border-radius:4px;background-color:#d9edf7;color:#3a87ad;position:relative;width:380px;z-index:1;border-color:#bce8f1;margin:auto;padding:8px 35px 8px 14px;}		</style>
	</head>
	<body>
		<h1><?php echo NAME; ?></h1>
		<?php if(isset($urlshoorten)) {?><p class="alert">Your URL is : <?php echo $urlshoorten; ?></p><?php } ?>
		<form action="index.php" method="post" class="box">
			<label for="url"><input type="url" name="url" id="url" required placeholder="Enter a long URL"/></label>
			  <input type="submit" value="Shorten"></input>
		</form>
		<p style="font-size: xx-small; text-align:right;">Powered by <a href="http://champlywood.free.fr/motapurls/index.php">MOTAPURLS</a></p>
	</body>
</html>
<?php } ?>